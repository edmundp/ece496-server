//
//  VirtualLightbulbActuator.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "VirtualLightbulbActuator.h"

#include <iostream>

void VirtualLightbulbActuator::init() {
    stateVariables[IS_ON] = StateVariable(TYPE_BOOL);
}

bool VirtualLightbulbActuator::transmitStateVariables(Pairing *pairing) {
    //Nothing to transmit since the data is already here
    
    bool isOn = stateVariables[IS_ON].getBoolValue();    
    //Can't turn on the lightbulb physically, so just print to terminal representing the action
    //of turning it on
    if (isOn) {
        std::cout << "Light On" << std::endl;
    }
    else {
        std::cout << "Light Off" << std::endl;
    }
    
    return true;
}

unsigned short VirtualLightbulbActuator::getActuatorStatus() {
    return 1;
}
