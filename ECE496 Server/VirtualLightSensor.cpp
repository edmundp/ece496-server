//
//  VirtualLightSensor.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "VirtualLightSensor.h"
#include <iostream>

void VirtualLightSensor::init() {
    stateVariables["lux"] = StateVariable(TYPE_FLOAT);
}

bool VirtualLightSensor::retrieveStateVariables() {
    float lux = 30.0 + (rand() % 10);

    stateVariables["lux"].setFloatValue(lux);
    
    return true;
}
