//
//  StateVariable.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "StateVariable.h"
#include <assert.h>

StateVariable::StateVariable(StateVariableType varType) {
    init(varType);
}

StateVariable::StateVariable(const StateVariable &var) {
    init(var.type);
    
    copyFrom(var);
}

StateVariable & StateVariable::operator= (const StateVariable &other) {
    copyFrom(other);
    return *this;
}

StateVariable::~StateVariable() {
    //Can't directly need a 'delete data' since the
    //compiler must know the type
    switch (type) {
        case TYPE_FLOAT:
            delete (float *)data;
            break;
        case TYPE_BOOL:
            delete (bool *)data;
            break;
        case TYPE_INT:
            delete (int *)data;
            break;
        case TYPE_STRING:
            delete (std::string *)data;
            break;
        default:
            break;
    }
    
    data = NULL;
}

void StateVariable::init(StateVariableType varType) {
    data = NULL;
    type = varType;
    
    switch (varType) {
        case TYPE_UNDEFINED: break;
        case TYPE_FLOAT:
            data = new float;
            *(float *)data = 0;
            break;
        case TYPE_BOOL:
            data = new bool;
            *(bool *)data = 0;
            break;
        case TYPE_INT:
            data = new int;
            *(int *)data = 0;
            break;
        case TYPE_STRING:
            data = new std::string;
            break;
        default:
            assert(0);
            break;
    }
}

void StateVariable::copyFrom(const StateVariable &var) {
    assert(type == TYPE_UNDEFINED || type == var.type);
    
    if (type == TYPE_UNDEFINED) {
        init(var.type);
    }
    
    type = var.type;
    
    switch (type) {
        case TYPE_FLOAT:
            *(float *)data = *(float *)var.data;
            break;
        case TYPE_BOOL:
            *(bool *)data = *(bool *)var.data;
            break;
        case TYPE_INT:
            *(int *)data = *(int *)var.data;
            break;
        case TYPE_STRING:
            *(std::string *)data = *(std::string *)var.data;
            break;
        default:
            break;
    }
}

float StateVariable::getFloatValue() {
    assert(type == TYPE_FLOAT);
    
    return *(float *)data;
}

void StateVariable::setFloatValue(float val) {
    assert(type == TYPE_FLOAT);
    
    *(float *)data = val;
}

int StateVariable::getIntValue() {
    assert(type == TYPE_INT);
    
    return *(int *)data;
}

void StateVariable::setIntValue(int val) {
    assert(type == TYPE_INT);
    
    *(int *)data = val;
}

bool StateVariable::getBoolValue() {
    assert(type == TYPE_BOOL);
    
    return *(bool *)data;
}

void StateVariable::setBoolValue(bool val) {
    assert(type == TYPE_BOOL);
    
    *(bool *)data = val;
}

std::string StateVariable::getStringValue() {
    assert(type == TYPE_STRING);
    
    return *(std::string *)data;
}

void StateVariable::setStringValue(std::string val) {
    assert(type == TYPE_STRING);
    
    *(std::string *)data = val;
}

std::ostream& operator<<(std::ostream& stream, const StateVariable& var) {
    void *data = var.data;
    
    switch (var.type) {
        case TYPE_FLOAT:
            stream << *(float *)data;
            break;
        case TYPE_BOOL:
            stream << *(bool *)data;
            break;
        case TYPE_INT:
            stream << *(int *)data;
            break;
        case TYPE_STRING:
            stream << *(std::string *)data;
            break;
        default:
            break;
    }
    
    return stream;
}
