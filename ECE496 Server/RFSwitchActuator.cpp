//
//  RFSwitchActuator.cpp
//  ECE496 Server
//
//  Created by E P on 11/13/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFSwitchActuator.h"

void RFSwitchActuator::init() {
    stateVariables[IS_ON] = StateVariable(TYPE_BOOL);
}

struct SwitchData {
    short isOn;
};

void RFSwitchActuator::setupTransmitData() {
    struct SwitchData *data = (struct SwitchData *)(&transmitData);
    data->isOn = stateVariables[IS_ON].getBoolValue();
}
