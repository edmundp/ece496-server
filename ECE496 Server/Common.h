//
//  Common.h
//  ECE496 Server
//
//  Created by E P on 11/12/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Common_h
#define ECE496_Server_Common_h

//Some common defines go in here

//We want to be able to compile this on both Mac OS X and Rasp Pi, however, we don't have
//access to the RF24 library and hardware on the Mac, so we disable RF functionality when
//compiling for Mac

//Check if this is a Rasp Pi (this check is not accurate but is sufficient
//for our needs)
#ifdef __arm__
    #define RASP_PI
#endif

//Amount of time to sleep after polling all the sensors
#define SENSOR_POLL_SLEEP_DURATION 2000

//Number of failed access attempts before we assume device is offline
#define DEVICE_FAILED_ATTEMPTS_THRESHOLD 3

/*
 Enable/Disable logging facility
 */

#define LOG_ENABLED 1
#ifdef LOG_ENABLED
#include "log.h"
#endif

/*
 Server Defines
 */

//Server's IP
#define SERVER_IP "localhost"

//Server's port
#define SERVER_PORT 2871

/*
 Device Defines
 */

//The length of the serial string
#define DEVICE_SERIAL_LENGTH 20

/*
 RF Defines
 */

//Name of the SPI device
#define SPI_DEVICE_NAME "/dev/spidev0.0"

//Pin for SPI's Chip Enable
#define CE_PIN 25

//Speed of SPI
#define SPI_SPEED 8000000

//Buffer size to transmit to and from server (in bytes)
#define PAYLOAD_SIZE 32

//Retry settings if connection error
#define RETRY_DELAY 15
#define RETRY_COUNT 15

//PA Level
#define PA_LEVEL RF24_PA_HIGH

//CRC Length
#define CRC_LENGTH RF24_CRC_16

//Network speed
#define DATA_RATE RF24_250KBPS

//The channel to communicate on
#define CHANNEL 100

//Number of milliseconds before a read transfer timeout
#define RX_TIMEOUT 100

#endif
