//
//  VirtualLightSensor.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFLightSensor.h"
#include <iostream>

void RFLightSensor::init() {
    stateVariables["lux"] = StateVariable(TYPE_FLOAT);
}

struct LightData {
    short success; //1 if successfully read sensor, 0 else
    short padding; //Int is 16 bits on Arduino, so add some padding
    float lux; //Light in lux
};

bool RFLightSensor::processRecievedData() {
    struct LightData *lightData = (struct LightData *)(&recievedData);
    
    if (lightData->success == 0) {
        //The data was invalid
        return false;
    }
    
    stateVariables["lux"].setFloatValue(lightData->lux);
    
    return true;
}
