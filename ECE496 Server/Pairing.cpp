//
//  Pairing.cpp
//  ECE496 Server
//
//  Created by Sumit K Chopra on 10/24/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "Pairing.h"
#include <stdlib.h>

int Pairing::currentID = 0;

std::string Pairing::getPairingAsString()
{
    std::string result;
    
    char str[1000]; //Is it good?
    sprintf(str,"%d %s [\"%d\" \"%s\" \"%s\" \"%s\"]\n",pairingID, actuator->getDeviceName().c_str(),firstCondition.getSensor()->getDeviceID(), firstCondition.getStateVariable().c_str(), firstCondition.getOperation().c_str(), firstCondition.getValue().c_str());

    result.append(str);
	if(numOfConditions == 2)
	{
		sprintf(str,"\"%s\" [\"%d\" \"%s\" \"%s\" \"%s\"]\n",OP.c_str(), secondCondition.getSensor()->getDeviceID(), secondCondition.getStateVariable().c_str(), secondCondition.getOperation().c_str(), secondCondition.getValue().c_str());
	}
	result.append(str);
    return result;
}

bool Pairing::areConditionsSatisfied()
{
	if(numOfConditions == 2)
	{
		if(OP == "and")
		{
			return firstCondition.isConditionStatisfied() && secondCondition.isConditionStatisfied();
		}
		else
		{
			return firstCondition.isConditionStatisfied() || secondCondition.isConditionStatisfied();
		}
	}
	else{
		printf("Checking first condition\n");
		return firstCondition.isConditionStatisfied();
	}
}
