//
//  log.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 1/9/2014.
//  Copyright (c) 2014 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_log_h
#define ECE496_Server_log_h

#include <iostream>
#include <fstream>
#include <ctime>

//#define LOG_PATH "./log.txt" //stores in the home directory for now, change this

class log {
	
public:
	static void error(std::string error);
	static void warning(std::string warning);
	static void msg(std::string msg);
	
private:
	void printTime();
	
};

#endif
