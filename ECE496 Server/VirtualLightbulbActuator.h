//
//  VirtualLightbulbActuator.h
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef __ECE496_Server__VirtualLightbulbActuator__
#define __ECE496_Server__VirtualLightbulbActuator__

#include "Actuator.h"

/*
 This actuator is for testing purposes only
 This implements a virtual lightbulb actuator that prints to terminal when turned on or off
 */

class VirtualLightbulbActuator:public Actuator
{
public:
    VirtualLightbulbActuator(std::string name, std::string description, std::string serial):Actuator(name,description,serial){};
    virtual bool transmitStateVariables(Pairing *pairing);
    virtual void init();
    virtual unsigned short getActuatorStatus();
};

#endif /* defined(__ECE496_Server__VirtualLightbulbActuator__) */
