//
//  VirtualMotionSensor.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFMotionSensor.h"
#include <iostream>
#include <sys/time.h>


void RFMotionSensor::init() {
    timeLastOn = 0;
    stateVariables["motiondetected"] = StateVariable(TYPE_BOOL);
}

struct MotionData {
    short motionDetected; //Was motion recently detected?
};

bool RFMotionSensor::processRecievedData() {
    struct MotionData *motionData = (struct MotionData *)(&recievedData);
    bool motionDetected = motionData->motionDetected;
    
    if (motionDetected) {
        timeLastOn = time(NULL);
    }
    else if (timeLastOn != 0) {
        //Check if we've exceed the timeout
        unsigned long int curTime = time(NULL);
        if ((curTime - timeLastOn) > MOTION_SENSOR_TIMEOUT) {
            motionDetected = false;
        }
        else {
            motionDetected = true;
        }
    }
    
    stateVariables["motiondetected"].setBoolValue(motionDetected);
    
    return true;
}
