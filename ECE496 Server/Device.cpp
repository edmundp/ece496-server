//
//  device.cpp
//  ECE496 Server
//
//  Created by Sumit K Chopra on 10/24/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "Device.h"

int Device::currentID = 0;

Device::Device(std::string name, std::string description, std::string serial)
{
    deviceID = currentID++;
    this->name = name;
    this->description = description;
    this->serial = serial;
    this->failedAccessAttempts = 0;
}

void Device::printStateVariables() {
    //Loop through the variables and print them
    
    typedef std::map<std::string, StateVariable>::iterator it;
    
    std::cout << "State Variables for device '" << name << "':" << std::endl;
    
    for (it iterator = stateVariables.begin(); iterator != stateVariables.end(); iterator++) {
        std::cout << '\t' << iterator->first << ": " << iterator->second << std::endl;
    }
    
    std::cout << std::endl;
}
