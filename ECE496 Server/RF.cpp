//
//  RF.cpp
//  ECE496 Server
//
//  Created by E P on 11/13/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RF.h"
#include <unistd.h>
#include <stdlib.h>
#include <fstream>

#ifdef RASP_PI

bool RF::hasInitialized = 0;

RF& RF::getInstance() {
    //Do some checks before we continue
    if (!hasInitialized) {
        //Are we root?
        if (getuid()) {
            std::cout << "ERROR: this program needs to be run in root (e.g. using sudo) in order to access the SPI hardware" << std::endl;
            exit(EXIT_FAILURE);
        }
        
        //Is the SPI device loaded?
        ifstream file(SPI_DEVICE_NAME);
        
        if (!file) {
            std::cout << "ERROR: this program requires the SPI device to be loaded (use 'gpio load spi')" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    
    hasInitialized = true;
    static RF instance;
    return instance;
}

void RF::setup() {
    std::cout << "RF Setup" << std::endl;
    
    radio.begin();
    radio.setRetries(RETRY_DELAY, RETRY_COUNT);
    radio.setPayloadSize(PAYLOAD_SIZE);
    radio.setAutoAck(1);
    radio.setDataRate(DATA_RATE);
    radio.setChannel(CHANNEL);
    radio.setPALevel(PA_LEVEL);
    radio.setCRCLength(CRC_LENGTH);
    
    radio.printDetails();
}

bool RF::tx(struct Data *data, uint64_t writePipe) {
    printf("RF::TX 0x%010llX... ", writePipe);

    radio.openWritingPipe(writePipe);
    
    //Hackish solution to fix the dropped RF packets issue
    //Don't know why it works, but it does
    //radio.write(0, 0);
    
    bool ok = radio.write(data, sizeof(struct Data));
    
    if (ok)
        std::cout << "success" << std::endl;
    else
        std::cout << "fail" << std::endl;
    
    return ok;
}

bool RF::rx(struct Data *data, uint64_t readPipe) {
    printf("RF::RX 0x%010llX... ", readPipe);
    
    radio.openReadingPipe(1, readPipe);
    radio.startListening();
    
    unsigned long start_time = __millis();
    
    bool timeout = false;
    
    while (true) {
        uint8_t pipe_num;
        if (radio.available(&pipe_num))
        {
            memset(data, 0, sizeof(struct Data));
            
            bool done = false;
            while (!done)
            {
                done = radio.read(data, sizeof(struct Data));
                
                delay(20);
            }
            
            break;
        }
        
        timeout = ((__millis() - start_time) > RX_TIMEOUT);
        if (timeout) {
            cout << "timed out" << endl;
            break;
        }
    }
    
    radio.stopListening();
    
    if (!timeout) {
        cout << "success" << endl;
    }
    
    return !timeout;
}

#endif
