//
//  RFLightSensor.h
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef __ECE496_Server__RFLightSensor__
#define __ECE496_Server__RFLightSensor__

/*
 This implements an RF light sensor
*/

#include "RFSensor.h"
#include <string>

class RFLightSensor:public RFSensor
{
public:
    RFLightSensor(std::string name, std::string description, std::string serial):RFSensor(name,description,serial){};
    virtual bool processRecievedData();
    virtual void init();
};

#endif /* defined(__ECE496_Server__RFLightSensor__) */
