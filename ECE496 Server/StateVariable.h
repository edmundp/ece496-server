//
//  StateVariable.h
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

// This class is for storing device state variables (i.e. properties) of devices
// Each type of device has its own set of state variables
// For example: a Temperature/Humidity sensor would have the
// following state variables: Temperature (float), Humidity (float)
// and a switch actuator may have: isOn (bool).
//
// Structuring it this way makes it possible to create a system that is general and
// can handle many different types of devices containg different types/amounts of data.

// TODO: Overload operators ==, <=, =>, etc. to allow comparisons
// Comparisons must be of the same type, e.g. can only compare float with float or bool with bool
// For bool and string, only == operator is valid

#ifndef __ECE496_Server__StateVariable__
#define __ECE496_Server__StateVariable__

#include <string>
#include <iostream>

//Common state variables
#define IS_ON "ison"

typedef enum {
    TYPE_UNDEFINED,
    TYPE_FLOAT, //4 bytes
    TYPE_INT, //4 bytes
    TYPE_BOOL, //1 byte
    TYPE_STRING //Arbitrary size
} StateVariableType;

class StateVariable {
    private:
        void *data;
        void init(StateVariableType varType);
        void copyFrom(const StateVariable &var);
    public:
        //Default initializer (type is undefined and must be set to a defined type before it can be used)
		StateVariable(){ init(TYPE_UNDEFINED);} ;
    
        //Initializer with type
        StateVariable(StateVariableType varType);
    
        //Assignment operator
        StateVariable & operator= (const StateVariable &other);
    
        //Copy constructor
        StateVariable(const StateVariable &var);
    
        //Destructor
        ~StateVariable();
    
        //Allow this to be passed into cout
        friend std::ostream& operator<<(std::ostream& stream, const StateVariable& var);
    
        //The variable type
        StateVariableType type;
    
        //Setters/getters (must call the correct one for its variable type)
        float getFloatValue();
        void setFloatValue(float val);
    
        int getIntValue();
        void setIntValue(int val);
    
        bool getBoolValue();
        void setBoolValue(bool val);
    
        std::string getStringValue();
        void setStringValue(std::string val);
};

#endif /* defined(__ECE496_Server__StateVariable__) */
