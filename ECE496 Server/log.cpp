//
//  log.cpp
//  ECE496 Server
//
//  Created by Sumit K Chopra on 1/9/2014.
//  Copyright (c) 2014 Sumit Kumar Chopra. All rights reserved.
//

#include "log.h"

void log::error(std::string error)
{
		
	std::cerr<<":"<<"[ERROR] "<<error;
	std::cerr<<std::endl;
}

void log::warning(std::string warning)
{
	//printTime();
	std::cerr<<":"<<"[WARN] "<<warning;
	std::cerr<<std::endl;
}

void log::msg(std::string msg)
{
	//printTime();
	std::cerr<<":"<<"[MSG] "<<msg;
	std::cerr<<std::endl;
}

void log::printTime()
{
	//just prints the date not time
	time_t t = time(0);   // get time now
    struct tm * now = localtime(&t);
	std::cerr << (now->tm_year + 1900)<<'-'<< (now->tm_mon + 1)<<'-'<<now->tm_mday;
}