//
//  VirtualTempSensor.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFTempSensor.h"
#include <iostream>

void RFTempSensor::init() {
    stateVariables["temp"] = StateVariable(TYPE_FLOAT);
    stateVariables["humidity"] = StateVariable(TYPE_FLOAT);
}

struct TempData {
    short success; //1 if successfully read sensor, 0 else
    short padding; //Int is 16 bits on Arduino, so add some padding
    float humidity; //Humidity in %
    float temp; //Temp in degrees C
};

bool RFTempSensor::processRecievedData() {
    struct TempData *tempData = (struct TempData *)(&recievedData);
    
    if (tempData->success == 0) {
        //The data was invalid
        return false;
    }
    
    stateVariables["temp"].setFloatValue(tempData->temp);
    stateVariables["humidity"].setFloatValue(tempData->humidity);
    
    return true;
}
