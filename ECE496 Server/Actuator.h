//
//  Actuator.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 2013-10-20.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Actuator_h
#define ECE496_Server_Actuator_h

#include "Device.h"

class Pairing;

// Virtual actuators should inherit from Actuator and RF actuators should inherit from RFActuator

class Actuator: public Device {
    
public:
    Actuator(std::string name, std::string description, std::string serial): Device(name,description,serial){};

    /*
     This transmits the currently stored state variables to the actuator
     When the actuator recieves the updated state variables, it should react on it's new values
     For example, if the IS_ON state variable of a switch actuator is changed and transmitted to the
     actuator, the actuator should immediately turn on or off depending on the new value of IS_ON
     
     The pairing that triggered the actuator is also passed in, which may be useful for some types
     of actuators (e.g. email)
     
     For an RF sensor, this would transmit these values wirelessly
     For a virtual sensor, the method of transmit varies
     
     Example of actuator state variables:
     IS_ON (most actuators will have this), lightColor (e.g. Philips Hue),
     lightBrightness (e.g. Philips Hue)
     
     Returns true if successful
     */
    virtual bool transmitStateVariables(Pairing *pairing) = 0;
    virtual void init() = 0;
    
    //Returns the current status
    //Only 2 statuses currently available:
    //not running or error = 0
    //online and running = 1
    virtual unsigned short getActuatorStatus() = 0;
};

#endif
