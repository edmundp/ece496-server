//
//  Server.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 2013-10-19.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Server_h
#define ECE496_Server_Server_h

#include <iostream>
#include "Sensor.h"
#include "Actuator.h"
#include <list>
#include <map>
#include <string>
#include "Condition.h"
#include "Pairing.h"
#include <vector>
#include <mutex>
#include "Common.h"

#define MAX_LISTENQUEUELEN 1	///< The maximum number of queued connections.
#define MAX_CMD_LEN 1024*2  //is it good?

#ifdef RASP_PI
#define BACKUP_FILE_PATH "./config_pi"
#else
#define BACKUP_FILE_PATH "./config"
#endif

class Server {

private:
    static int clientsock; //SOCKET for the client (initialized in run())
    //pairings map sensor, actuator, condition
	
public:
    static std::map<int, Sensor*> sensors;//list of sensors
    static std::map<int, Actuator*> actuators;//list of actuators
    static std::list<Pairing> pairings;
	
	//file stream for backup
	static FILE *fp;
	static std::mutex lock_sensors_map;
	static std::mutex lock_actuators_map;
    static std::mutex lock_pairings_map;
    static std::mutex lock_rf;
    
public:
    static void run(std::string ipAddress,int port);
    static std::string getLocalIPAddress();
    static int handle_command(std::string receivedText, bool isBackup);
	static void loadFromBackup();
	
private:
    static int sendall(const char *buf, const size_t len);
    static int receiveLine(char *buf, const size_t buflen);
    
    
    //SERVICE INTERFACE: interface to client (all of these will send data direct to client using sendall)
    static void setStateVariable(Actuator &actuator, std::string stateVariable, std::string stateVariableValue);
    static void getListOfSensors();
    static void getListOfActuators();
    static void getListOfPairings();
    static void createPairing(std::string receivedText, bool isBackup); //need to design condition
    static void getStateVariable(std::string receivedText);
	static void addDevice(std::string receivedText, bool isBackup);
	static void removeDevice(std::string receivedText, bool isBackup);
	static void getStateVariableForActuator(std::string receivedText);
    
    //helpers
    static std::string getValueForStateVariable(int sensorID,std::string stateVariableStr);
};

#endif
