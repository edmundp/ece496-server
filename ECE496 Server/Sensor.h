//
//  Sensor.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 2013-10-20.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Sensor_h
#define ECE496_Server_Sensor_h


#include "Device.h"

// Virtual sensors should inherit from Sensor and RF sensors should inherit from RFSensor

class Sensor:public Device
{
public:
    Sensor(std::string name, std::string description, std::string serial):Device(name,description,serial){};
    
    /*
     This updates the currently stored state variables by retrieving values from the sensor
     For an RF sensor, this would fetch these values wirelessly
     For a virtual sensor, the method of fetching varies
     
     Examples of sensor state variables:
     Light (float), temperature (float), humidity (float), motionDetected (bool)
     
     Returns true if successful
     */
    virtual bool retrieveStateVariables() = 0;
    virtual void init() = 0;
};

#endif
