//
//  VirtualEmailActuator.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "VirtualEmailActuator.h"
#include "Pairing.h"

#include <iostream>
#include <cstdlib>

void VirtualEmailActuator::init() {
    stateVariables["to_email"] = StateVariable(TYPE_STRING);
    stateVariables["message"] = StateVariable(TYPE_STRING);
    
    stateVariables[IS_ON] = StateVariable(TYPE_BOOL);
    
    //Hardcoded for now
    stateVariables["to_email"].setStringValue(std::string("edmund.phung@mail.utoronto.ca"));
    
    prevOnVal = false;
}

bool VirtualEmailActuator::transmitStateVariables(Pairing *pairing) {
    /*
     Mail command format example:
     echo "Test text" | mail -s "Test Mail" edmund.phung@mail.utoronto.ca &
     The ampersand at the end is to make it asychronous
     Requires SSMTP to be installed and setup with some SMTP server (such as Gmail)
     */
    
    bool shouldEmail = false;
    
    if (prevOnVal != stateVariables[IS_ON].getBoolValue()) {
        if (!prevOnVal) {
            shouldEmail = true;
        }
        
        prevOnVal = stateVariables[IS_ON].getBoolValue();
    }
    std::cerr<<"[DEBUG] about to send email\n";
    if (shouldEmail) {
        std::cerr<<"[DEBUG] sending email\n";
        
        std::string subject = "Home Automation Notification";
        
        std::string message;
        
        if (pairing) {
            message += "Pairing " + std::to_string(pairing->getPairingID()) + " has been triggered!\n";
            message += pairing->getPairingAsString();
        }
        else {
            message += "Email actuator triggered!";
        }
        
        std::string to_email = stateVariables["to_email"].getStringValue();
        
        std::string command;
        command += "echo \"";
        command += message;
        command += "\" | mail -s \"";
        command += subject;
        command += "\" ";
        command += to_email;
        command += " &";
        
        std::cout << "Command: " + command << std::endl;
        
        system(command.c_str());
    }
    
    return true;
}

unsigned short VirtualEmailActuator::getActuatorStatus() {
    return 1;
}
