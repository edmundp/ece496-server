//
//  RF.h
//  ECE496 Server
//
//  Created by E P on 11/13/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

// Singleton class for common RF functions and variables

#ifndef __ECE496_Server__RF__
#define __ECE496_Server__RF__

#include <iostream>
#include "Common.h"

#ifdef RASP_PI
    #include "./Libraries/librf24/RF24.h"
#endif

struct Data {
    char data[PAYLOAD_SIZE];
};

#ifdef RASP_PI

class RF {
private:
    RF():radio(SPI_DEVICE_NAME, SPI_SPEED, CE_PIN) { setup(); };
    RF(RF const&);
    void operator=(RF const&);
    void setup();
public:
    static RF& getInstance();
    static bool hasInitialized;
    RF24 radio;
    
    //Pipes are 40 bit values
    bool tx(struct Data *data, uint64_t writePipe);
    bool rx(struct Data *data, uint64_t readPipe);
};

#endif


#endif /* defined(__ECE496_Server__RF__) */
