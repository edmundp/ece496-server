//
//  Pairing.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 10/24/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Pairing_h
#define ECE496_Server_Pairing_h

#include <list>
#include "Actuator.h"
#include "Condition.h"


//NOTE: Only supports two conditons for

class Pairing
{
private:
    int pairingID;
    Actuator *actuator;
    Condition firstCondition;
    Condition secondCondition;
    std::string OP;
    static int currentID;
    int numOfConditions; //1 or 2
    
public:
    Pairing(){pairingID = currentID++;};
    Pairing(Actuator *actuator,Condition firstCondition){pairingID = currentID++; this->actuator =  actuator; this->firstCondition = firstCondition; numOfConditions = 1;};
    Pairing(Actuator *actuator,Condition firstCondition,std::string OP,Condition secondCondition){pairingID = currentID++; this->actuator =  actuator; this->firstCondition = firstCondition; this->OP = OP; this->secondCondition = secondCondition; numOfConditions = 2;};
    
    int getPairingID(){return pairingID;};
    int getActuatorID() {return actuator->getDeviceID();};
    Condition getFirstCondition(){return firstCondition;};
    Condition getSecondCondition(){return secondCondition;};
    int getNumOfConditons(){return numOfConditions;};
    std::string getOP(){return OP;};
    std::string getPairingAsString();
    
    bool areConditionsSatisfied(); //check sensors and respond true if conditions are satisfied
    
private:
    //helper functions
    bool evaluateANDList();
    bool evaluateORList();
};

#endif
