//
//  Condition.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 10/24/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Condition_h
#define ECE496_Server_Condition_h

#include "Sensor.h"

class Condition
{
private:
    Sensor *sensor;
    std::string OP; // possible OP <, >, ==, check ENUMS???
    std::string stateVariable;
    //double value;
    std::string value;
    
public:
    
    Condition(){};
    Condition(Sensor *sensor, std::string stateVariable, std::string OP, std::string value){this->sensor = sensor; this->stateVariable = stateVariable; this->OP = OP; this->value = value; };
    
    Sensor* getSensor() {return sensor;};
    std::string getOperation() {return OP;};
    std::string getStateVariable() {return stateVariable;};
    std::string getValue() {return value;};
    bool isConditionStatisfied();
};

#endif
