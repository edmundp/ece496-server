#!usr/bin/perl

print "Forking process script\n";

@args = ("sudo ./server");


while (1)
{
	system(@args);

	if ($? == -1) 
	{
		print "failed to execute: $?\n";
		break;
	}
	elsif ($? & 127)
	{
		printf "child died with signal %d, %s coredump\n",
			($? & 127), ($? & 128) ? 'with' : 'without';
	}
	elsif ($? == 130)
	{
		printf "killed by ctr-C\n";
		break;
	}
	else
	{
		printf "child exited with value %d\n", $? >> 8;
	}
}
