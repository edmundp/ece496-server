//
//  RFActuator.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFActuator.h"
#include "Pairing.h"
#include "Encrypt.h"

#ifdef RASP_PI

static unsigned short sequenceNum = 1;

struct ActuatorTransmitData {
    char data[28];
    unsigned short command;
    unsigned short seqNum;
};

struct ActuatorRecieveData {
    unsigned short status;
    char data[30];
};

//The real class that's used on the Rasp Pi
bool RFActuator::transmitStateVariables(Pairing *pairing) {
    memset(&transmitData, 0, sizeof(struct Data));
    memset(&recievedData, 0, sizeof(struct Data));
    
    setupTransmitData();
    
    //Add in a sequence number
    ActuatorTransmitData *data = (ActuatorTransmitData *)&transmitData;
    data->seqNum = sequenceNum;
    data->command = 0;
    
    //Encrypt the data
    //printHex((char *)(&transmitData), sizeof(struct Data));
    XORCipher((char *)(&transmitData), sizeof(struct Data), (char *)(&writePipe), sizeof(writePipe));
    //printHex((char *)(&transmitData), sizeof(struct Data));
    
    int retries = 2;
    for (int i = 0; i < retries; i++) {
        //Send state variables to actuator
        if (RF::getInstance().tx(&transmitData, writePipe)) {
            //Read reply data from actuator
            if (RF::getInstance().rx(&recievedData, readPipe)) {
                sequenceNum++;
                return true;
            }
        }
    }
    
    return false;
}

unsigned short RFActuator::getActuatorStatus() {
    memset(&transmitData, 0, sizeof(struct Data));
    memset(&recievedData, 0, sizeof(struct Data));
    
    //Add in a sequence number
    ActuatorTransmitData *data = (ActuatorTransmitData *)&transmitData;
    data->seqNum = sequenceNum;
    data->command = 1;
    
    //Encrypt the data
    //printHex((char *)(&transmitData), sizeof(struct Data));
    XORCipher((char *)(&transmitData), sizeof(struct Data), (char *)(&writePipe), sizeof(writePipe));
    //printHex((char *)(&transmitData), sizeof(struct Data));
    
    int retries = 2;
    for (int i = 0; i < retries; i++) {
        //Send status request to actuator
        if (RF::getInstance().tx(&transmitData, writePipe)) {
            //Read reply data from actuator
            if (RF::getInstance().rx(&recievedData, readPipe)) {
                sequenceNum++;
                
                ActuatorRecieveData *rx = (ActuatorRecieveData *)(&recievedData);
                unsigned short status = rx->status;
                return status;
            }
        }
    }
    
    return 0;
}

#else

bool RFActuator::transmitStateVariables(Pairing *pairing) {
    return false;
}

unsigned short RFActuator::getActuatorStatus() {
    return 0;
}

#endif

