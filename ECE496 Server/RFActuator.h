//
//  RFActuator.h
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef __ECE496_Server__RFActuator__
#define __ECE496_Server__RFActuator__

#include "Common.h"
#include "Actuator.h"
#include "RF.h"
#include <assert.h>
#include <sstream>

class RFActuator:public Actuator {
protected:
    uint64_t readPipe;
    uint64_t writePipe;
    struct Data transmitData;
    struct Data recievedData;
public:
    RFActuator(std::string name, std::string description, std::string serial): Actuator(name,description,serial) {
        /*
         Every sensor/actuator device has a globally unique read and write address for RF communication
         Since the serial number is unique for every device, we define the serial number to be the concatenation
         of the read and write address of the device, where both the read and write address is in hex
         */
        
        //Make sure it's the correct length
        assert(serial.length() == DEVICE_SERIAL_LENGTH);
        
        //Split the string exactly in half, where the first part is the read pipe address
        //and the second part is the write pipe address
        int halfLength = DEVICE_SERIAL_LENGTH / 2;
        
        std::string readPart = serial.substr(0, halfLength);
        std::string writePart = serial.substr(halfLength, halfLength);
        
        //Convert it from string to int
        std::stringstream ss1;
        ss1 << std::hex << readPart;
        ss1 >> readPipe;
        
        std::stringstream ss2;
        ss2 << std::hex << writePart;
        ss2 >> writePipe;
    };
    
    //This transmit the data stored in 'transmitData' to the actuator and retrieve some reply data
    //in 'recievedData'
    virtual bool transmitStateVariables(Pairing *pairing);
    
    virtual unsigned short getActuatorStatus();
    
    /*
     Functions required to be implemented in subclasses
     */
    
    //In this function, subclasses should take the current state variables
    //and store them into 'transmitData'
    virtual void setupTransmitData() = 0;
    virtual void init() = 0;
};

#endif /* defined(__ECE496_Server__RFActuator__) */
