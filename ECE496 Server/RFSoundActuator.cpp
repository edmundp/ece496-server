//
//  RFSoundActuator.cpp
//  ECE496 Server
//
//  Created by E P on 11/13/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFSoundActuator.h"

void RFSoundActuator::init() {
    stateVariables[IS_ON] = StateVariable(TYPE_BOOL);
    stateVariables["sound_id"] = StateVariable(TYPE_INT);
}

struct SoundData {
    short isOn;
    short soundID;
};

void RFSoundActuator::setupTransmitData() {
    struct SoundData *data = (struct SoundData *)(&transmitData);
    data->isOn = stateVariables[IS_ON].getBoolValue();
}
