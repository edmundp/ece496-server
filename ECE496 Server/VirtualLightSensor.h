//
//  VirtualLightSensor.h
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef __ECE496_Server__VirtualLightSensor__
#define __ECE496_Server__VirtualLightSensor__

/*
 This sensor is for testing purposes only
 This implements a virtual light sensor that generates random data
*/

#include "Sensor.h"
#include <string>

class VirtualLightSensor:public Sensor
{
public:
    VirtualLightSensor(std::string name, std::string description, std::string serial):Sensor(name,description,serial){};
    virtual bool retrieveStateVariables();
    virtual void init();
};

#endif /* defined(__ECE496_Server__VirtualLightSensor__) */
