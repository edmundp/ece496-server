# Raspberry Pi

## How to Build and Install
1. Compile and install wiringPi (in ECE496 Server/Libraries directory)
2. Compile and install librf24 (in ECE496 Server/Libraries directory)
3. Install g++-4.7 (sudo apt-get install g++-4.7)
4. Make the server (run Make in ECE496 Server directory)

## How to Run
1. Load SPI device by running command “gpio load spi” (this needs to be done on every reboot, TODO: auto load this)
2. cd into ECE496 Server and sudo ./server (it requires sudo to access low level hardware)

TODO: Write a script that automates this entire process

NOTE: some steps may be missing, I need to test this on a fresh install of Raspbian

# Mac OS X
Only virtual sensors and actuators will work on the Mac version.

## How to Build and Run
1. Buld and run in Xcode
