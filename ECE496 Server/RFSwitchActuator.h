//
//  RFSwitchActuator.h
//  ECE496 Server
//
//  Created by E P on 11/13/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef __ECE496_Server__RFSwitchActuator__
#define __ECE496_Server__RFSwitchActuator__

/*
 This implements an RF switch actuator
 */

#include "RFActuator.h"
#include <string>

class RFSwitchActuator:public RFActuator
{
public:
    RFSwitchActuator(std::string name, std::string description, std::string serial):RFActuator(name,description,serial){};
    virtual void setupTransmitData();
    virtual void init();
};

#endif /* defined(__ECE496_Server__RFSwitchActuator__) */
