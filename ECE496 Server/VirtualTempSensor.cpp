//
//  VirtualTempSensor.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "VirtualTempSensor.h"
#include <iostream>

void VirtualTempSensor::init() {
    stateVariables["temp"] = StateVariable(TYPE_FLOAT);
    stateVariables["humidity"] = StateVariable(TYPE_FLOAT);
	status = true;
}

bool VirtualTempSensor::retrieveStateVariables() {
    float newTemp = 25.0 + (rand() % 10);
    float newHumidity = 50.0 + (rand() % 10);

    stateVariables["temp"].setFloatValue(newTemp);
    stateVariables["humidity"].setFloatValue(newHumidity);
    
    return true;
}
