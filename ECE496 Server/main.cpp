//
//  main.cpp
//  ECE496 Server
//
//  Created by Sumit K Chopra on 2013-10-19.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
#include <thread>
#include <mutex>
#include <execinfo.h>
#include <signal.h>

#include "Common.h"
#include "Server.h"
#include "Actuator.h"
#include "Server.h"

#include "VirtualTempSensor.h"
#include "VirtualLightSensor.h"
#include "VirtualLightbulbActuator.h"
#include "RFTempSensor.h"
#include "RFLightSensor.h"
#include "RFSwitchActuator.h"
#include "RFMotionSensor.h"
#include "RFSoundActuator.h"
#include "VirtualEmailActuator.h"

/*
 global object for logging, initialize in main
 */

#ifdef LOG_ENABLED
#include "log.h"
#endif

//Crash handler
void crashHandler(int sig) {
    void *array[10];
    size_t size;
    
    // get void*'s for all entries on the stack
    size = backtrace(array, 10);
    
    // print out all the frames to stderr
    fprintf(stderr, "Error: signal %d:\n", sig);
    backtrace_symbols_fd(array, size, STDERR_FILENO);
    exit(1);
}


int main(int argc, char *argv[])
{
    //Install the crash handler
    signal(SIGSEGV, crashHandler);

    //Print initial message
    std::cerr << "Home Automation Server" << std::endl;
    std::cerr << "Built on " << __DATE__ << " " << __TIME__ << std::endl;

	std::cerr <<"Initializing Server"<<std::endl;
	Server::loadFromBackup();

    /*
     TESTING CODE (remove when done)
     */
    
    //These virtual sensors and actuators are for testing only
    
	/*
    #ifndef RASP_PI
    VirtualTempSensor *sensor1 = new VirtualTempSensor("Virtual Temp", "test", "12345478900987654323");
    sensor1->init();
    Server::sensors[sensor1->getDeviceID()] = sensor1;
    
    VirtualLightSensor *sensor2 = new VirtualLightSensor("Virtual Light", "test", "12325678910987654324");
    sensor2->init();
    Server::sensors[sensor2->getDeviceID()] = sensor2;
    
    VirtualLightbulbActuator *actuator1 = new VirtualLightbulbActuator("Virtual Bulb", "test", "13345678920987654325");
    actuator1->init();
    Server::actuators[actuator1->getDeviceID()] = actuator1;
    #endif
    
    RFSwitchActuator *actuator2 = new RFSwitchActuator("RF_Switch1", "test", "d6d169179b" "161f9bb9cd");
    actuator2->init();
    Server::actuators[actuator2->getDeviceID()] = actuator2;
    
    RFSwitchActuator *actuator5 = new RFSwitchActuator("RF_Switch2", "test", "663162579b" "76ef93d9cd");
    actuator5->init();
    Server::actuators[actuator5->getDeviceID()] = actuator5;
    
    RFSwitchActuator *actuator6 = new RFSwitchActuator("RF_Switch3", "test", "465166779b" "564f9879cd");
    actuator6->init();
    Server::actuators[actuator6->getDeviceID()] = actuator6;
    
    RFSoundActuator *actuator3 = new RFSoundActuator("RF_Sound", "test", "2a198158b6" "8eefbc2303");
    actuator3->init();
    Server::actuators[actuator3->getDeviceID()] = actuator3;
    
    VirtualEmailActuator *actuator4 = new VirtualEmailActuator("Virtual_Email", "test", "55555544545225555564");
    actuator4->init();
    Server::actuators[actuator4->getDeviceID()] = actuator4;
     
    RFMotionSensor *sensor5 = new RFMotionSensor("RF_Motion", "test", "8cbe3dd8f7" "495b292523");
    sensor5->init();
    Server::sensors[sensor5->getDeviceID()] = sensor5;
    
    RFTempSensor *sensor3 = new RFTempSensor("RF_Temp", "test", "67fa663280" "8f240a0101");
    sensor3->init();
    Server::sensors[sensor3->getDeviceID()] = sensor3;

    RFLightSensor *sensor4 = new RFLightSensor("RF_Light", "test", "cc13168aa5" "40948e92cd");
    sensor4->init();
    Server::sensors[sensor4->getDeviceID()] = sensor4;
     */
     
    char *ipAddress = (char *)SERVER_IP;
    int port = SERVER_PORT;
    
    //Check arguments for IP and Port
    if (argc == 3) {
        ipAddress = argv[1];
        port = atoi(argv[2]);
    }
    
    //RUN server listening for commands
    std::thread serverThread(&Server::run, ipAddress, port);
	
    std::cerr << "Server now running on " << ipAddress << ":" << SERVER_PORT << std::endl << std::endl;
    std::cerr << "Now polling sensors..." << std::endl << std::endl;
    
    /*
    int i = 0;
    int num = 3;
    while (1) {
        RFSwitchActuator *sw[] = {actuator2,actuator5,actuator6};
        //RFSwitchActuator *sw[] = {actuator5};
        
        for (int j = 0; j < num; j++) {
            sw[j]->getStateVariables()[IS_ON].setBoolValue(false);
            sw[j]->transmitStateVariables(NULL);
        }
        
        std::cout << "Turn on " << i << std::endl;
        
        sw[i]->getStateVariables()[IS_ON].setBoolValue(true);
        sw[i]->transmitStateVariables(NULL);
        
        i++;
        i = i % num;
        
        std::this_thread::sleep_for(std::chrono::milliseconds(4000));
    }
     */
    
    //RUN sensor polling loop
    while (1)
    {
        typedef std::map<int, Sensor*>::iterator it;
        
        Server::lock_rf.lock();
        
        //Poll sensors for data
		Server::lock_sensors_map.lock();
		
        std::cout << "--------------------------------" << std::endl;
        std::cout << "Sensor Poll" << std::endl;
        std::cout << "--------------------------------" << std::endl << std::endl;
        
        for (it iterator = Server::sensors.begin(); iterator != Server::sensors.end(); iterator++) {
            //Fetch the values of the state variables
            Sensor *sensor = iterator->second;
            std::cout << "Polling " << sensor->getDeviceName() << " for data" << std::endl;
            
            if (sensor->retrieveStateVariables()) {
                sensor->setFailedAccessAttempts(0);
                sensor->setStatus(true);
                
                sensor->printStateVariables();
            }
            else {
                sensor->setFailedAccessAttempts(sensor->getFailedAccessAttempts() + 1);
                
                if (sensor->getFailedAccessAttempts() >= DEVICE_FAILED_ATTEMPTS_THRESHOLD) {
                    std::cout << "Recieve error: Device is offline" << std::endl;
                    sensor->setStatus(false);
                }
                else {
                    std::cout << "Recieve error" << std::endl;
                }
            }
            
            std::cout << std::endl;
        }
        
		Server::lock_sensors_map.unlock();
        
		
        //Poll actuators for status (check if they're online)
        
		Server::lock_actuators_map.lock();
		
        std::cout << "--------------------------------" << std::endl;
        std::cout << "Actuator Poll" << std::endl;
        std::cout << "--------------------------------" << std::endl << std::endl;
        
        typedef std::map<int, Actuator*>::iterator it2;
        for (it2 iterator = Server::actuators.begin(); iterator != Server::actuators.end(); iterator++) {
            //Check if the actuator is online
            Actuator *actuator = iterator->second;
            std::cout << "Polling " << actuator->getDeviceName() << " for status" << std::endl;
            
            
            unsigned short status = 0;
            if ((status = actuator->getActuatorStatus())) {
                actuator->setFailedAccessAttempts(0);
                actuator->setStatus(true);
                std::cout << "Actuator is online" << std::endl;
            }
            else {
                actuator->setFailedAccessAttempts(actuator->getFailedAccessAttempts() + 1);
                
                if (actuator->getFailedAccessAttempts() >= DEVICE_FAILED_ATTEMPTS_THRESHOLD) {
                    std::cout << "Recieve error: Device is offline" << std::endl;
                    actuator->setStatus(false);
                }
                else {
                    std::cout << "Recieve error" << std::endl;
                }
            }
            
            std::cout << std::endl;
        }
        
		Server::lock_actuators_map.unlock();
        
        
        //Check if the current state variables meet any of the pairing conditions
        //change to just check the pairings for the sensor
        //right now checking all the pairings
        
        std::cout << "--------------------------------" << std::endl;
        std::cout << "Pairing Check" << std::endl;
        std::cout << "--------------------------------" << std::endl << std::endl;
        
        Server::lock_pairings_map.lock();
        
        int numPairings = Server::pairings.size();
        
        if (numPairings) {
            //We want the OR of conditions
            //e.g. Suppose we have two pairings:
            //if lux > 20 then turn on switch
            //if temp > 20 then turn on switch
            //We want: if (temp > 20 or lux > 20) then turn on switch
            //Basically, if any pairings turn on an actuator, no other pairing can
            //turn it off

            //Initialize all to off
            for(std::list<Pairing>::iterator it2 = Server::pairings.begin();it2 != Server::pairings.end();it2++)
            {
                Server::actuators[it2->getActuatorID()]->getStateVariables()[IS_ON].setBoolValue(false);
            }
            
            //Now turn on the correct ones
            for(std::list<Pairing>::iterator it2 = Server::pairings.begin();it2 != Server::pairings.end();it2++)
            {
                std::cout << "Checking pairing " << it2->getPairingID() << "... ";
                //sensor->printStateVariables();
                if(it2->areConditionsSatisfied())
                {
                    std::cout << "satisfied" << std::endl;
                    Server::actuators[it2->getActuatorID()]->getStateVariables()[IS_ON].setBoolValue(true);
                    std::cerr<<"Switching " + Server::actuators[it2->getActuatorID()]->getDeviceName() + " to ON\n";
                    //Server::actuators[it2->getActuatorID()]->transmitStateVariables(&(*it2));
                }
                else
                {
                    std::cout << "unsatisfied" << std::endl;
                    //std::cerr<<"Switching to off\n";
                    //Server::actuators[it2->getActuatorID()]->getStateVariables()[IS_ON].setBoolValue(false);
                    //Server::actuators[it2->getActuatorID()]->transmitStateVariables(&(*it2));
                }
            }
            
            for(std::list<Pairing>::iterator it2 = Server::pairings.begin();it2 != Server::pairings.end();it2++)
            {
                Server::actuators[it2->getActuatorID()]->transmitStateVariables(&(*it2));
            }
        }
        
        
        Server::lock_pairings_map.unlock();
        
        Server::lock_rf.unlock();
        
        //Sleep for a short duration so we don't kill the CPU
        std::this_thread::sleep_for(std::chrono::milliseconds(SENSOR_POLL_SLEEP_DURATION));
    }
    
    serverThread.join();
	
	return EXIT_SUCCESS;
}


