//
//  server.cpp
//  ECE496 Server
//
//  Created by Sumit K Chopra on 2013-10-19.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "Server.h"
#include "Pairing.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <assert.h>
#include "StateVariable.h"
#include "Common.h"


#include "VirtualTempSensor.h"
#include "VirtualLightSensor.h"
#include "VirtualLightbulbActuator.h"
#include "RFTempSensor.h"
#include "RFLightSensor.h"
#include "RFSwitchActuator.h"
#include "RFMotionSensor.h"
#include "RFSoundActuator.h"
#include "VirtualEmailActuator.h"

int Server::clientsock = 0; //SOCKET for the client (initialized in run())
std::map<int, Sensor*> Server::sensors;//list of sensors
std::map<int, Actuator*> Server::actuators;//list of actuators
std::list<Pairing> Server::pairings;
FILE * Server::fp;
std::mutex Server::lock_sensors_map;
std::mutex Server::lock_actuators_map;
std::mutex Server::lock_pairings_map;
std::mutex Server::lock_rf;


void Server::loadFromBackup()
{
	Server::fp=fopen(BACKUP_FILE_PATH, "r");
	if(Server::fp == NULL)
	{
		std::cerr<<"ERROR opening the backup file"<<std::endl;
		return;
	}
	
	char line [2000];
    while(fgets(line,sizeof(line),Server::fp)!= NULL) /* read a line from a file */ {
		Server::handle_command(line, true);
    }
	fclose(fp);
}

void Server::setStateVariable(Actuator &actuator,std::string stateVariableStr, std::string stateVariableValue)
{
    std::cerr<<"[DEBUG] Actuator "<<actuator.getDeviceID()<<std::endl;
    
    StateVariableType type = actuator.getStateVariables()[stateVariableStr].type;
    
    switch (type) {
        case TYPE_FLOAT:
            actuator.getStateVariables()[stateVariableStr].setFloatValue(atof(stateVariableValue.c_str()));
            break;
        case TYPE_BOOL:
            assert(stateVariableValue == "true" || stateVariableValue == "false");
            if(stateVariableValue == "true")
            {
                std::cerr<<"[DEBUG] setting state variable to true\n"<<std::endl;
                actuator.getStateVariables()[stateVariableStr].setBoolValue(true);
                std::cerr<<"[DEBUG] stateVariable set to "<<actuator.getStateVariables()[stateVariableStr].getBoolValue()<<std::endl;
            }
            else
            {
                std::cerr<<"[DEBUG] setting state variable to false\n"<<std::endl;
                actuator.getStateVariables()[stateVariableStr].setBoolValue(false);
            }
            break;
        case TYPE_INT:
            actuator.getStateVariables()[stateVariableStr].setIntValue(atoi(stateVariableValue.c_str()));
            break;
        case TYPE_STRING:
            actuator.getStateVariables()[stateVariableStr].setStringValue(stateVariableValue);
            break;
        default:
            break;
    }
    
    Server::lock_rf.lock();
    actuator.transmitStateVariables(NULL);
    Server::lock_rf.unlock();
    
	std::string response  = "setStateVariable:Done\n";
	sendall(response.c_str(),response.length());
    
}

void Server::getListOfSensors()
{
    
    std::string response;
    response = "GetListOfSensorsResponse:";
    std::string size = std::to_string(sensors.size());
    response+=size+":";
    
    std::map<int,Sensor*>::iterator it;
    for(it = sensors.begin();it!=sensors.end();it++)
    {
        Sensor *sensor = it->second;
        response+=std::to_string(sensor->getDeviceID());
        response+="|";
        response+=sensor->getSerial();
        response+="|";
        response+=sensor->getDeviceName();
        response+="|";
        response+=std::to_string(sensor->getStatus());
        response+="|";
        response+=sensor->getDescription();
        response+=":";
        response+=std::to_string(sensor->getStateVariables().size());
        
        for(std::map<std::string,StateVariable>::iterator it2 = sensor->getStateVariables().begin();it2 !=  sensor->getStateVariables().end();it2++)
        {
			response+="|";
            response+=it2->first;
			response+="|";
            response+=std::to_string(it2->second.type);
        }
        response+="!";
    }
    response+="\n";
    sendall(response.c_str(),response.length());
}

void Server::getListOfActuators()
{
	int result;
    std::string response;
    response = "GetListOfActuatorsResponse:";
    std::string size = std::to_string(actuators.size());
    response+=size+":";
    
    std::map<int,Actuator*>::iterator it;
    for(it = actuators.begin();it!=actuators.end();it++)
    {
        Actuator *actuator = it->second;
        response+=std::to_string(actuator->getDeviceID());
        response+="|";
        response+=actuator->getSerial();
        response+="|";
        response+=actuator->getDeviceName();
        response+="|";
        response+=std::to_string(actuator->getStatus());
        response+="|";
        response+=actuator->getDescription();
        response+=":";
        response+=std::to_string(actuator->getStateVariables().size());
        
        for(std::map<std::string,StateVariable>::iterator it2 = actuator->getStateVariables().begin();it2 !=  actuator->getStateVariables().end();it2++)
        {
			response+="|";
            response+=it2->first;
			response+="|";
            response+=std::to_string(it2->second.type);
        }
        response+="!";
    }
    response+="\n";
	
	
    result = sendall(response.c_str(),response.length());
	printf("result = %d\n",result);
	if(result < 0)
	{
#ifdef LOG_ENABLED
		log::error("error occurred while sending response for getListOfActuators()\n");
#endif
		std::cerr <<"error occurred while sending response for getListOfActuators()\n"<<std::endl;
	}
}

void Server::createPairing(std::string receivedText, bool isBackup)
{
    std::string response;
    
    int actuatorID = atoi(receivedText.substr(0,receivedText.find(":")).c_str());
    receivedText.erase(0, receivedText.find(":") + 1);
    Actuator *actuator = actuators[actuatorID];
    
    int numOfConditons = atoi(receivedText.substr(0,receivedText.find(":")).c_str());
    receivedText.erase(0, receivedText.find(":") + 1);
    
    int sensorID = atoi(receivedText.substr(0,receivedText.find("|")).c_str());
    receivedText.erase(0, receivedText.find("|") + 1);
    Sensor *sensor = sensors[sensorID];
    
    std::string stateVariable = receivedText.substr(0,receivedText.find("|")).c_str();
    receivedText.erase(0, receivedText.find("|") + 1);
    
    std::string operation = receivedText.substr(0,receivedText.find("|"));
    receivedText.erase(0, receivedText.find("|") + 1);
    
    std::string value = receivedText.substr(0,receivedText.find("!")).c_str();
    receivedText.erase(0, receivedText.find("!") + 1);
	
    Condition firstCondition(sensor, stateVariable, operation, value);
    
    if(numOfConditons == 2)
    {
        std::string OP = receivedText.substr(0,receivedText.find("|"));
        receivedText.erase(0, receivedText.find("|") + 1);
    
        sensorID = atoi(receivedText.substr(0,receivedText.find("|")).c_str());
        receivedText.erase(0, receivedText.find("|") + 1);
        sensor = sensors[sensorID];
    
        stateVariable = receivedText.substr(0,receivedText.find("|")).c_str();
        receivedText.erase(0, receivedText.find("|") + 1);
    
        operation = receivedText.substr(0,receivedText.find("|"));
        receivedText.erase(0, receivedText.find("|") + 1);
    
        value = receivedText.substr(0,receivedText.find("!")).c_str();
        receivedText.erase(0, receivedText.find("!") + 1);
    
        Condition secondCondition(sensor, stateVariable, operation, value);
        //StateVariableType type = sensor->getStateVariables()[stateVariable].type;
        
        Pairing pairing(actuator,firstCondition, OP, secondCondition);
        std::cerr<<"[DEBUG] "<< pairing.getPairingAsString()<< "created.\n";
        
        lock_pairings_map.lock();
        pairings.push_back(pairing);
        lock_pairings_map.unlock();
        
        response.append("createPairingResponse:Done:");
        response.append(std::to_string(pairing.getPairingID()));
    }
    else
    {
		
        Pairing pairing(actuator,firstCondition);
        std::cerr<<"[DEBUG] "<<pairing.getPairingAsString()<<" created.\n";
        
        lock_pairings_map.lock();
        pairings.push_back(pairing);
        lock_pairings_map.unlock();
        
        response.append("createPairingResponse:Done:");
        response.append(std::to_string(pairing.getPairingID()));
    }
    
    response.append("!");
	response+="\n";
	if(!isBackup)
	{
		sendall(response.c_str(),response.length());
	}
}

void Server::getListOfPairings()
{
    std::string response;
    response.append("getListOfPairingsResponse:");
    
    lock_pairings_map.lock();
    
    size_t size = pairings.size();
    response.append(std::to_string(size)+":");
    
    std::list<Pairing>::iterator it;
    for(it = pairings.begin();it!=pairings.end();it++)
    {
        response.append(std::to_string(it->getPairingID())+":"+std::to_string(it->getActuatorID()));
        response.append(":"+std::to_string(it->getNumOfConditons())+":"+std::to_string(it->getFirstCondition().getSensor()->getDeviceID())+"|"+ it->getFirstCondition().getStateVariable()+"|"+it->getFirstCondition().getOperation()+"|"+it->getFirstCondition().getValue());
        response.append("!");
        
        size_t numberOfConditions = it->getNumOfConditons();
        if(numberOfConditions != 1)
        {
            response.append(it->getOP());
            response.append("|"+std::to_string(it->getSecondCondition().getSensor()->getDeviceID())+"|"+it->getSecondCondition().getStateVariable()+"|"+it->getSecondCondition().getOperation()+"|"+it->getSecondCondition().getValue());
            response.append("!");
        }
    }
    
    lock_pairings_map.unlock();
    
    response+="\n";
	sendall(response.c_str(),response.length());
}

void Server::getStateVariable(std::string receivedText)
{
    int sensorID = atoi(receivedText.substr(0,receivedText.find(":")).c_str());
    receivedText.erase(0, receivedText.find("|") + 1);
    
    std::string stateVariableStr = receivedText.substr(0,receivedText.find("!"));
    receivedText.erase(0, receivedText.find("!") + 1);
    
    std::string value = getValueForStateVariable(sensorID,stateVariableStr);
    
    std::string response;
    
    response.append("getStateVariableResponse:"+value+"!");
    response+="\n";
	sendall(response.c_str(),response.length());
    
}


void Server::addDevice(std::string receivedText, bool isBackup)
{
	// inputs
	// type: VirtualTempSensor, VirtualLightSensor, RFTempSensor, RFLightSensor, RFMotionSensor
	// VirtualEmailActuator, VirtualLightBulbActuator, RFSwicthActuator, RFSoundActuator
	// Name:
	// Description:
	// Serial:
	
	// return the device ID
	//addDevice:virtualtempsensor virtTemp test 4345678909876543212!
	//addDevice:virtuallightsensor virtLightSensor test 5345678909876543212!
	//addDevice:virtualemailactuator virtEmail test 5445678909876543212!
	//addDevice:virtuallightbulbactuator virtBiulb test 6545678909876543212!
	//getlistofsensors:
	//getlistofactuators:
	//removeDevice:1!
	//getlistofsensors:
	//removeDevice:3!
	//getlistofactuators:
	
#ifdef LOG_ENABLED
	log::msg("CALL addDevice");
#endif
	
	int deviceID = 0;
	
	std::string type = receivedText.substr(0,receivedText.find("|"));
	receivedText.erase(0, receivedText.find("|") + 1);
	
	std::string Name = receivedText.substr(0,receivedText.find("|"));
	receivedText.erase(0, receivedText.find("|") + 1);
	
	std::string description = receivedText.substr(0,receivedText.find("|"));
	receivedText.erase(0, receivedText.find("|") + 1);
	
	std::string serial = receivedText.substr(0,receivedText.find("!"));
	receivedText.erase(0, receivedText.find("|") + 1);
	
	if(type == "virtualtempsensor")
	{
		VirtualTempSensor *sensor = new VirtualTempSensor(Name, description, serial);
		sensor->init();
		Server::lock_sensors_map.lock();
		Server::sensors[sensor->getDeviceID()] = sensor;
		Server::lock_sensors_map.unlock();
		deviceID = sensor->getDeviceID();
	}
	else if (type == "virtuallightsensor")
	{
		VirtualLightSensor *sensor = new VirtualLightSensor(Name, description, serial);
		sensor->init();
		Server::lock_sensors_map.lock();
		Server::sensors[sensor->getDeviceID()] = sensor;
		Server::lock_sensors_map.unlock();
		deviceID = sensor->getDeviceID();
	}
	else if (type == "rftempsensor")
	{
		RFTempSensor *sensor = new RFTempSensor(Name, description, serial);
		sensor->init();
		Server::lock_sensors_map.lock();
		Server::sensors[sensor->getDeviceID()] = sensor;
		Server::lock_sensors_map.unlock();
		deviceID = sensor->getDeviceID();
	}
	else if (type == "rflightsensor")
	{
		RFLightSensor *sensor = new RFLightSensor(Name, description, serial);
		sensor->init();
		Server::lock_sensors_map.lock();
		Server::sensors[sensor->getDeviceID()] = sensor;
		Server::lock_sensors_map.unlock();
		deviceID = sensor->getDeviceID();
	}
	else if (type == "rfmotionsensor")
	{
		RFMotionSensor *sensor = new RFMotionSensor(Name, description, serial);
		sensor->init();
		Server::lock_sensors_map.lock();
		Server::sensors[sensor->getDeviceID()] = sensor;
		Server::lock_sensors_map.unlock();
		deviceID = sensor->getDeviceID();
	}
	else if (type == "virtualemailactuator")
	{
		VirtualEmailActuator *actuator = new VirtualEmailActuator(Name, description, serial);
		actuator->init();
		Server::lock_actuators_map.lock();
		Server::actuators[actuator->getDeviceID()] = actuator;
		Server::lock_actuators_map.unlock();
		deviceID = actuator->getDeviceID();
	}
	else if (type == "virtuallightbulbactuator")
	{
		VirtualLightbulbActuator *actuator = new VirtualLightbulbActuator(Name, description, serial);
		actuator->init();
		Server::lock_actuators_map.lock();
		Server::actuators[actuator->getDeviceID()] = actuator;
		Server::lock_actuators_map.unlock();
		deviceID = actuator->getDeviceID();
	}
	else if (type == "rfswitchactuator")
	{
		RFSwitchActuator *actuator = new RFSwitchActuator(Name, description, serial);
		actuator->init();
		Server::lock_actuators_map.lock();
		Server::actuators[actuator->getDeviceID()] = actuator;
		Server::lock_actuators_map.unlock();
		deviceID = actuator->getDeviceID();
	}
	else if (type == "rfsoundactuator")
	{
		RFSoundActuator *actuator = new RFSoundActuator(Name, description, serial);
		actuator->init();
		Server::lock_actuators_map.lock();
		Server::actuators[actuator->getDeviceID()] = actuator;
		Server::lock_actuators_map.unlock();
		deviceID = actuator->getDeviceID();
	}
	
	if(!isBackup)
	{
		std::string response;
		response.append("addDeviceResponse:Done:"+std::to_string(deviceID)+"!");
		response+="\n";
		sendall(response.c_str(),response.length());
	}
}

void Server::removeDevice(std::string receivedText, bool isBackup)
{
#ifdef LOG_ENABLED
	log::msg("CALL removeDevice");
#endif
	
	// inputs
	// device ID:
	// return: OK
	
	int deviceID = atoi(receivedText.substr(0,receivedText.find("!")).c_str());
	
	if(sensors.find(deviceID) != sensors.end())
	{
		sensors.erase(sensors.find(deviceID));
	}
	else if (actuators.find(deviceID) != actuators.end())
	{
		actuators.erase(actuators.find(deviceID));
	}
	else{
		std::cerr<<"device with ID "<<deviceID<<" does not exist"<<std::endl;
	}
	
	std::string response("removeDeviceResponse:Done:"+std::to_string(deviceID)+"!");
	response = response+"\n";
	sendall(response.c_str(),response.length());
}

void Server::getStateVariableForActuator(std::string receivedText)
{
	int actuatorID = atoi(receivedText.substr(0,receivedText.find(":")).c_str());
    receivedText.erase(0, receivedText.find("|") + 1);
    
    std::string stateVariableStr = receivedText.substr(0,receivedText.find("!"));
    receivedText.erase(0, receivedText.find("!") + 1);
    
    //std::string value = getValueForStateVariable(sensorID,stateVariableStr);
    StateVariable stateVariable = actuators[actuatorID]->getStateVariables()[stateVariableStr];
	
	StateVariableType type = stateVariable.type;
	if(type != TYPE_BOOL)
	{
		std::cerr<<"ERROR state variable not of type BOOL"<<std::endl;
	}
	
	std::string value = stateVariable.getBoolValue()? "true":"false";
	
	
    std::string response;
    
    response.append("getStateVariableForActuatorResponse:"+value+"!");
    response+="\n";
	sendall(response.c_str(),response.length());
}


void Server::run(std::string ipAddress,int port)
{
    int status;
    
    // Create a socket.
	int listensock = socket(PF_INET, SOCK_STREAM, 0);
	if (listensock < 0) {
        std::cerr<<"[Error] creating socket.\n";
		//exit(EXIT_FAILURE);
	}
    
	// Allow listening port to be reused if defunct.
	int yes = 1;
	status = setsockopt(listensock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes);
	if (status != 0) {
        std::cerr<<"[Error] configuring socket.\n";
		//exit(EXIT_FAILURE);
	}
    
	// Bind it to the listening port.
	struct sockaddr_in listenaddr;
	memset(&listenaddr, 0, sizeof listenaddr);
	listenaddr.sin_family = AF_INET;
	listenaddr.sin_port = htons(port);
	inet_pton(AF_INET, ipAddress.c_str(), &(listenaddr.sin_addr)); // bind to local IP address
	status = ::bind(listensock, (struct sockaddr*) &listenaddr, sizeof(listenaddr));
	if (status != 0) {
        std::cerr<<"[Error] binding socket.\n";
		//exit(EXIT_FAILURE);
	}
    
	// Listen for connections.
	status = listen(listensock, MAX_LISTENQUEUELEN);
	if (status != 0) {
        std::cerr<<"Error listening on socket.\n";
		//exit(EXIT_FAILURE);
	}
    
    // Listen loop.
	int wait_for_connections = 1;
	while (wait_for_connections) {
		// Wait for a connection.
		struct sockaddr_in clientaddr;
		socklen_t clientaddrlen = sizeof clientaddr;
		clientsock = accept(listensock, (struct sockaddr*)&clientaddr, &clientaddrlen);
		if (clientsock < 0) {
			printf("Error accepting a connection.\n");
			exit(EXIT_FAILURE);
		}
        std::cerr<<"Got a connection from "<<inet_ntoa(clientaddr.sin_addr)<<":"<<clientaddr.sin_port<<".\n";
        
		// Get commands from client.
		int wait_for_commands = 1;
		do {
			// Read a line from the client.
			char cmd[MAX_CMD_LEN];
			int status = receiveLine(cmd,MAX_CMD_LEN);
			if (status != 0) {
				// Either an error occurred or the client closed the connection.
				wait_for_commands = 0;
			} else {
				// Handle the command from the client.
				int status = handle_command(cmd, false);
				if (status != 0)
					wait_for_commands = 0; // Oops.  An error occured.
			}
		} while (wait_for_commands);
		
		// Close the connection with the client.
		close(clientsock);
        std::cerr<<"Closed connection from "<<inet_ntoa(clientaddr.sin_addr)<<":"<<clientaddr.sin_port<<"\n";
	}
    
	// Stop listening for connections.
	close(listensock);
}

std::string Server::getLocalIPAddress() {
    // Get the server's local IP address
    int fd;
    struct ifreq ifr;
    
    fd = socket(AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;
    
    // Note: this assumes the device is connected to the network using
    // "en1" on the Mac and "wlan0" on the Rasp Pi
    #ifdef RASP_PI
        strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);
    #else
        strncpy(ifr.ifr_name, "en1", IFNAMSIZ-1);
    #endif
    
    ioctl(fd, SIOCGIFADDR, &ifr);
    
    close(fd);
    
    return std::string(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

int Server::sendall(const char *buf, const size_t len)
{
	size_t tosend = len;
    ssize_t bytes;
	while (tosend > 0) {
			bytes = write(clientsock, buf, tosend);
		
		if (bytes <= 0)
		{
            #ifdef LOG_ENABLED
			log::error("[ERROR] send was unsuccessful\n");
            #endif
			std::cerr<<"[ERROR] send was unsuccessful\n";
			break; // send() was not successful, so stop.
		}
		tosend -= (size_t) bytes;
		buf += bytes;
	};
	return tosend == 0 ? 0 : -1;
}

int Server::receiveLine(char *buf, const size_t buflen)
{
	int status = 0; // Return status.
	size_t bufleft = buflen;
    //char *buf = (char*)malloc(sizeof(char)*bufleft);
    //strcpy(buf, str.c_str());
	while (bufleft > 1) {
		// Read one byte from scoket.
		ssize_t bytes = recv(clientsock, buf, 1, 0);
		if (bytes <= 0) {
			// recv() was not successful, so stop.
			status = -1;
			break;
		} else if (*buf == '\n') {
			// Found end of line, so stop.
			*buf = 0; // Replace end of line with a null terminator.
			status = 0;
			break;
		} else {
			// Keep going.
			bufleft -= 1;
			buf += 1;
		}
	}
	*buf = 0; // add null terminator in case it's not already there.
    
	return status;
}

int Server::handle_command(std::string receivedText, bool isBackup)
{
    std::transform(receivedText.begin(), receivedText.end(), receivedText.begin(), ::tolower);
    std::string functionToCall = receivedText.substr(0,receivedText.find(":"));
    receivedText.erase(0, receivedText.find(":") + 1);
    
    if(functionToCall == "setstatevariable")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL setStateVariable");
        #endif
		std::cerr<<"CALL setStateVariable\n";
        std::string token = receivedText.substr(0,receivedText.find("|"));
        receivedText.erase(0, receivedText.find("|") + 1);
        int actuatorID = atoi(token.c_str());
        Actuator *actuator = actuators[actuatorID];
        
        std::string stateVariableStr = receivedText.substr(0,receivedText.find("|")).c_str();
        receivedText.erase(0, receivedText.find("|") + 1);
        
        std::string stateVariableValue = receivedText.substr(0,receivedText.find("!")).c_str();
        
        printf("[DEBUG] actuatorID = %d, stateVariableStr = \"%s\", stateVariableValue = \"%s\"\n", actuatorID,stateVariableStr.c_str(), stateVariableValue.c_str());
        
        setStateVariable(*actuator,stateVariableStr, stateVariableValue);
    }
    else if(functionToCall == "getlistofsensors")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL getListOfSensors");
        #endif
		std::cerr<<"CALL getListOfSensors"<<std::endl;
        getListOfSensors();
    }
    else if(functionToCall == "getlistofactuators")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL getListOfActuators");
        #endif
        getListOfActuators();
		printf("done sending getlistofactuators\n");
    }
    else if(functionToCall == "getlistofpairings")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL getListOfPairings");
        #endif
        getListOfPairings();
        
    }
    else if(functionToCall == "createpairing")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL createPairing");
        #endif
        
		createPairing(receivedText, isBackup);
		
		if(!isBackup)
		{
			Server::fp = fopen(BACKUP_FILE_PATH,"a");
			if(fp == NULL)
			{
				std::cerr<<"ERROR opening backup file"<<std::endl;
			}
			std::string temp = "createpairing:"+receivedText+"\n";
			fputs(temp.c_str(),fp);
			fclose(fp);
		}
    }
    else if(functionToCall == "getsensorhistorydata")
    {
        //NOT IMPLEMENTED
    }
    else if(functionToCall == "getstatevariable")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL getStateVariable");
        #endif
        getStateVariable(receivedText);
    }
    else if(functionToCall == "removepairing")
    {
        #ifdef LOG_ENABLED
		log::msg("CALL removePairing");
        #endif
        int pairingID = atoi(receivedText.substr(0,receivedText.find(":")).c_str());
        
        lock_pairings_map.lock();
        
        Pairing pairingToRemove;
        for(std::list<Pairing>::iterator it=pairings.begin();it!=pairings.end();it++)
        {
            if(it->getPairingID() == pairingID)
            {
                pairings.erase(it);
                break;
            }
        }
        
        lock_pairings_map.unlock();
        
		if(!isBackup)
		{
			std::string response("removePairingResponse:Done:"+std::to_string(pairingID)+"!");
			response = response+"\n";
			sendall(response.c_str(),response.length());
			
			Server::fp = fopen(BACKUP_FILE_PATH,"a");
			if(fp == NULL)
			{
				std::cerr<<"ERROR opening backup file"<<std::endl;
			}
			std::string temp = "removepairing:"+receivedText+"\n";
			fputs(temp.c_str(),fp);
			fclose(fp);
		}
    }
	else if (functionToCall == "adddevice")
	{
		
		addDevice(receivedText, isBackup);
		
		if(!isBackup)
		{
			Server::fp = fopen(BACKUP_FILE_PATH,"a");
			if(fp == NULL)
			{
				std::cerr<<"ERROR opening backup file"<<std::endl;
			}
			std::string temp = "adddevice:"+receivedText+"\n";
			fputs(temp.c_str(),fp);
			fclose(fp);
		}
		
	}
	else if (functionToCall == "removedevice")
	{
		removeDevice(receivedText, isBackup);
		
		if(!isBackup)
		{
			Server::fp = fopen(BACKUP_FILE_PATH,"a");
			if(fp == NULL)
			{
				std::cerr<<"ERROR opening backup file"<<std::endl;
			}
			std::string temp = "removedevice:"+receivedText+"\n";
			fputs(temp.c_str(),fp);
			fclose(fp);
		}
	}
	else if (functionToCall == "getstatevariableforactuator")
	{
		getStateVariableForActuator(receivedText);
	}
    else
    {
        #ifdef LOG_ENABLED
		log::error("UNRECOGNIZED COMMAND");
        #endif
		receivedText = "ERROR: unrecognized command \"" + receivedText +"\"\n";
        sendall(receivedText.c_str(),receivedText.length());
    }
	return 0;
}

//Helpers
std::string Server::getValueForStateVariable(int sensorID,std::string stateVariableStr)
{
    StateVariable stateVariable = sensors[sensorID]->getStateVariables()[stateVariableStr];
    StateVariableType type = stateVariable.type;

    switch (type)
    {
        case TYPE_FLOAT:
            return std::to_string(stateVariable.getFloatValue());
        case TYPE_BOOL:
            if(stateVariable.getBoolValue())
            {
                return "true";
            }
            else
            {
                return "false";
            }
            break;
        case TYPE_INT:
            return std::to_string(stateVariable.getIntValue());
        case TYPE_STRING:
            return stateVariable.getStringValue();
        default:
            std::cerr<<"[ERROR] Type Unknown\n"<<std::endl;
            break;
    }
    
    return "[ERROR] Type Unknown";
}

