//
//  RFMotionSensor.h
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef __ECE496_Server__RFMotionSensor__
#define __ECE496_Server__RFMotionSensor__

/*
 This implements an RF Motion sensor
*/

#include "RFSensor.h"
#include <string>

// After detecting motion, amount of time of inactivity before sensor
// should report no motion (this must be at least as large as the
// actual timeout on the physical sensor)
#define MOTION_SENSOR_TIMEOUT 10

class RFMotionSensor:public RFSensor
{
private:
    unsigned long int timeLastOn;
public:
    RFMotionSensor(std::string name, std::string description, std::string serial):RFSensor(name,description,serial){};
    virtual bool processRecievedData();
    virtual void init();
};

#endif /* defined(__ECE496_Server__RFMotionSensor__) */
