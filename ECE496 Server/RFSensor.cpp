//
//  RFSensor.cpp
//  ECE496 Server
//
//  Created by E P on 11/11/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "RFSensor.h"

#ifdef RASP_PI

//The real class that's used on the Rasp Pi

//Data structure for sending request command to sensor
struct RequestData {
    int commandCode;
};

bool RFSensor::retrieveStateVariables() {
    int retries = 2;
    for (int i = 0; i < retries; i++) {
        //Setup the request data structure
        struct Data txData;
        memset(&txData, 0, sizeof(struct Data));
        memset(&recievedData, 0, sizeof(struct Data));
        
        struct RequestData *reqData = (struct RequestData *)(&txData);
        reqData->commandCode = 1;
        
        //Send request to sensor for data
        if (RF::getInstance().tx(&txData, writePipe)) {
            //Read data from sensor
            if (RF::getInstance().rx(&recievedData, readPipe)) {
                if (processRecievedData()) {
                    return true;
                }
                else {
                    //This is expected for some types of sensors
                    //e.g. temp sensor because there is a minimum
                    //sampling time interval required in order
                    //for the data to be correct
                    cout << "Sensor data invalid" << endl;
                }
            }
        }
    }
    
    return false;
}

#else

//Fake class
bool RFSensor::retrieveStateVariables() {
    return false;
}

#endif
