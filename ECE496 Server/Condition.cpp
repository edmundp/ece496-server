//
//  Condition.cpp
//  ECE496 Server
//
//  Created by Sumit K Chopra on 11/14/2013.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#include "Condition.h"
#include <stdlib.h>

bool Condition::isConditionStatisfied()
{
	
    //printf("[DEBUG] state variable = %s, sensorID = %d, value = %d\n",stateVariable.c_str(), sensor->getDeviceID(),sensor->getStateVariables()[stateVariable].getBoolValue());
    StateVariableType type = sensor->getStateVariables()[stateVariable].type;
	printf("STATE VARIABLE = \"%s\", stateVariableType = %d\n",stateVariable.c_str(), type);
    
    switch (type) {
        case TYPE_FLOAT:
            if(OP == ">")
            {
                return sensor->getStateVariables()[stateVariable].getFloatValue() > atof(value.c_str());
            }
            else if(OP == "<")
            {
                return sensor->getStateVariables()[stateVariable].getFloatValue() < atof(value.c_str());
            }
            else if(OP == "==")
            {
                return sensor->getStateVariables()[stateVariable].getFloatValue() == atof(value.c_str());
            }
            else
            {
                std::cout<<"[ERROR] OP not supported\n"<<std::endl;
            }
            break;
        case TYPE_BOOL:
            
            if(OP == "==")
            {
				printf("OP = ==, value = \"%s\"\n",value.c_str());
                if(value == "true")
                {
					printf("setting motionDetected to TRUE\n");
                    return sensor->getStateVariables()[stateVariable].getBoolValue() == true;
                }
                else
                {
					printf("setting motionDetected to FALSE\n");
                    return sensor->getStateVariables()[stateVariable].getBoolValue() == false;
                }
            }
            else
            {
                std::cout<<"[ERROR] OP not supported\n"<<std::endl;
            }
            break;
        case TYPE_INT:
            if(OP == ">")
            {
                return sensor->getStateVariables()[stateVariable].getFloatValue() > atoi(value.c_str());
            }
            else if(OP == "<")
            {
                return sensor->getStateVariables()[stateVariable].getFloatValue() < atoi(value.c_str());
            }
            else if(OP == "==")
            {
                return sensor->getStateVariables()[stateVariable].getFloatValue() == atoi(value.c_str());
            }
            else
            {
                std::cout<<"[ERROR] OP not supported\n"<<std::endl;
            }
            break;
        case TYPE_STRING:
            if(OP == "==")
            {
                return sensor->getStateVariables()[stateVariable].getStringValue() == value;
            }
            else
            {
                std::cout<<"[ERROR] OP not supported\n"<<std::endl;
            }
            break;
        default:
            std::cout<<"[ERROR] type unknown for state variable"<<std::endl;
            break;
    }
    return false;
}
