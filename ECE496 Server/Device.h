//
//  Device.h
//  ECE496 Server
//
//  Created by Sumit K Chopra on 2013-10-20.
//  Copyright (c) 2013 Sumit Kumar Chopra. All rights reserved.
//

#ifndef ECE496_Server_Device_h
#define ECE496_Server_Device_h

#include <string>
#include <map>
#include "StateVariable.h"

class Device
{
private:
    //Device's current ID
    int deviceID;
    
    //Counter for generating next ID
    static int currentID;
    
    //Device's name
    std::string name;
    
    //Device's description
    std::string description;
    
    //Device's unique serial number
    std::string serial;
    
    //Failed access attempts (number of times server unsuccessfully tried
    //to contact this device)
    //Gets reset back to zero on successful contact
    int failedAccessAttempts;

protected:
    //Device's current state variables
    std::map<std::string, StateVariable> stateVariables;
	
	//Is device on and connected?
    bool status; //ON = True
    
public:
    Device(std::string name, std::string description, std::string serial);
    
    int getDeviceID(){return deviceID;};
    
    void setDeviceName(std::string name) {this->name = name;};
    void setDescription(std::string description) {this->description = description;};
    void setSerial(std::string serial) {this->serial = serial;};
    void setFailedAccessAttempts(int val) {this->failedAccessAttempts = val; };
    void setStatus(bool status) {this->status = status;};
    
    
    std::string getDeviceName(){return name;};
    std::string getDescription(){return description;};
    std::string getSerial(){ return serial; };
    std::map<std::string, StateVariable> &getStateVariables(){return stateVariables;};
    int getFailedAccessAttempts() { return failedAccessAttempts; };
    
    bool getStatus() {return status;};
    
    
    //Initialization of the state variables and other things should be done here
    virtual void init() = 0;
    
    //For debugging purposes, prints out all the state variables of the device
    void printStateVariables();
};

#endif
